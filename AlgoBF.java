import java.util.Arrays;

/* brute-force algorithm: generate all permutations of the job sequence and 
 * find the one with minimal makespan */
class AlgoBF {
	static Schedule min;
	static Schedule tmp;

	static Schedule run(Job[] jobs) {
		min = new Schedule(jobs);
		tmp = new Schedule(jobs);
		min.set();

		generate(tmp.n);
		return min;
	}

	/*
	 * recursively generate all permutations of the first N elements of tmp.list
	 */
	private static void generate(int N) {

		if (N == 1) { // next permutation
			tmp.set();

			if (min.makespan() > tmp.makespan()) { // select if smaller
				min.start = Arrays.copyOf(tmp.start, tmp.n);
				min.list = Arrays.copyOf(tmp.list, tmp.n);
			}
			return;
		}

		for (int i = 0; i < N; i++) {
			swap(i, N - 1); // move job i to the end
			generate(N - 1); // permutations of jobs without job i
			swap(i, N - 1); // swap back
		}
	}

	private static void swap(int i, int j) {
		Job job = tmp.list[i];
		tmp.list[i] = tmp.list[j];
		tmp.list[j] = job;
	}
}