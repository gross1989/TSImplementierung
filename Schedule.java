import java.util.Arrays;
import java.util.Collections;

/* data type to save a job schedule with operations to set jobs and return makespan */
class Schedule {

	int n; // number of jobs
	Job[] list; // job sequence
	double[] start; // start times

	/* constructor through job array */
	Schedule(Job[] jobs) {
		n = jobs.length;
		list = Arrays.copyOf(jobs, n);
		start = new double[n];
	}

	/* set all jobs one after the other (O(n^2)) time) */
	void set() {
		set(0, n);
	}

	/* set jobs between position from (inclusive) and to (exclusive) */
	void set(int from, int to) {
		for (int i = from; i < to; i++) {
			set(i);
		}
	}

	/* set job k assuming jobs 0 to k-1 have been set correctly (O(n)) time) */
	void set(int k) {

		double t = 0; // earliest possible start time

		for (int i = 0; i < k; i++) {
			double diff = list[i].diff(list[k]);
			t = Math.max(t, start[i] + diff);
		}
		start[k] = t;
	}

	/* return makespan of whole schedule (O(n)) time) */
	double makespan() {
		return makespan(n);
	}

	/* return makespan up to position to (exclusive) */
	double makespan(int to) {

		double max = 0; // latest time value

		for (int i = 0; i < to; i++) {
			double size_horizontal = 0;
			if (list[i] instanceof JobIsosceles) {
				size_horizontal = ((JobIsosceles) list[i]).s;

			} else if (list[i] instanceof JobRectangular) {
				size_horizontal = ((JobRectangular) list[i]).w;

			} else if (list[i] instanceof JobEquilateral) {
				size_horizontal = ((JobEquilateral) list[i]).s;

			} else if (list[i] instanceof JobCircle) {
				size_horizontal = ((JobCircle) list[i]).r * 2;
			}
			max = Math.max(max, start[i] + size_horizontal);
		}
		return max;
	}

	/* bt_ratio <= 2 implies greedy algorithm delivers optimal schedule */
	static double bt_ratio(JobIsosceles[] jobs) {
		Arrays.sort(jobs, Collections.reverseOrder());
		double max = 0;
		for (int i = 2; i <= jobs.length; i++) {
			double s1 = jobs[i - 1].s;
			double s2 = jobs[(int) Math.ceil(i / 2) - 1].s;
			max = Math.max(max, s2 / s1);
		}
		return max;
	}
}