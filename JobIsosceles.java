import java.awt.Polygon;

/* the simple isosceles shaped job */
class JobIsosceles extends Job implements Comparable<JobIsosceles> {

	double s; // cathetus length

	JobIsosceles(double side) {
		s = side;
	}

	public int compareTo(JobIsosceles b) {
		return Double.compare(s, b.s);
	}

	@Override
	/* minimal difference between this isosceles and another job b */
	public double diff(Job b) {

		return new JobRectangular(s, s).diff(b);
	}

	@Override
	public String toString() {
		return Double.toString(s);
	}

	public Polygon visual(double scale) {

		int[] xs = {0, 0, (int) (s * scale)};
		int[] ys = {0, -(int) (s * scale), -(int) (s * scale)};

		return new Polygon(xs, ys, 3);
	}
}