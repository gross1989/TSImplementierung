import java.awt.Polygon;

/* equilateral shaped job */
class JobEquilateral extends Job {

	double s; // side length
	double h; // height

	JobEquilateral(double side) {
		s = side;
		h = Math.sqrt(3) / 2 * s;
	}

	@Override
	public String toString() {
		return String.format("(%f,%f)", h, s);
	}

	@Override
	/* minimal difference between this equilateral and another job b */
	public double diff(Job b) {

		return .5 * s + new JobRectangular(.5 * s, h).diff(b);
	}

	public Polygon visual(double scale) {

		int[] xs = {0, (int) (.5 * s * scale), (int) (s * scale)};
		int[] ys = {-(int) (h * scale), 0, -(int) (h * scale)};

		return new Polygon(xs, ys, 3);
	}
}