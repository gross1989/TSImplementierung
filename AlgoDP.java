import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/* dynamic programming algorithm with approximation ratio (1+eps): round up 
 * job sizes and start times and memorize makespans for subproblems */
class AlgoDP {

	static double[] sizes; // rounded job sizes
	static double T; // smallest time unit
	static double S; // smallest size unit
	static Map<int[], Double> dictionary; // memorize subproblem results
	static int[] choices; // record choices made during recursion

	static Schedule run(JobIsosceles[] jobs, double eps) {

		int n = jobs.length;

		int Z = (int) Math.ceil(Math.log(n / eps) / Math.log(1 + eps)) + 1;
		sizes = new double[Z];
		for (int i = 0; i < Z; i++) {
			sizes[i] = Math.pow(1 + eps, i);
		}

		double s1 = 0;
		for (JobIsosceles j : jobs) {
			s1 = Math.max(s1, j.s); // biggest job size
		}
		S = s1 / sizes[Z - 1];
		T = s1 * eps / n;

		// save set S of jobs sizes and configuration C of latest start times as
		// [size_1, amount_1, time_slot_1, size_2, amount_2, time_slot_2, ... ]
		// with time_slot -1 means not set yet
		int[] CS = new int[3 * n];
		int index = 0;

		outer : for (JobIsosceles job : jobs) {

			double new_size = job.s / S;
			int exp = Math.min(Z - 1,
					(int) Math.ceil(Math.log(new_size) / Math.log(1 + eps)));

			for (int j = 0; j < index; j += 3) {
				if (CS[j] == exp) {
					CS[j + 1] += 1; // increase amount by one
					continue outer;
				}
			}
			CS[index++] = exp; // append size with amount one
			CS[index++] = 1;
			CS[index++] = -1;
		}
		CS = Arrays.copyOf(CS, index);

		dictionary = new HashMap<int[], Double>();
		choices = new int[n];
		F(CS);

		// System.out.println("Z " + Z + " sizes " + Arrays.toString(sizes));
		// System.out.println("T " + T + " S " + S);
		// System.out.println("CS " + Arrays.toString(CS));
		// System.out.println("res " + res);
		// System.out.println(dictionary.size());
		// System.out.println("choice " + Arrays.toString(choices));

		List<JobIsosceles> tmp_list = new LinkedList<>(Arrays.asList(jobs));
		for (int i = 0; i < n; i++) {

			double above = sizes[choices[i]] * S;
			double below = choices[i] > 0 ? sizes[choices[i] - 1] * S : 0;

			for (JobIsosceles job : tmp_list) {

				if (job.s <= above && job.s > below) {
					jobs[i] = job; // get job left over fitting in size
					tmp_list.remove(job);
					break;
				}
			}
		}

		Schedule sched = new Schedule(jobs);
		sched.set();

		return sched;
	}

	// returns optimal makespan for rounded job sizes
	static private double F(int[] CS) {
		return F(CS, 0);
	}

	static private double F(int[] CS, int rec_level) {

		Double result = dictionary.get(CS);

		if (result != null) {
			return result;

		} else {
			boolean S_empty = true;
			for (int i = 0; i < CS.length; i += 3) {
				if (CS[i + 1] != 0) {
					S_empty = false;
					break;
				}
			}
			if (S_empty) { // not in dictionary and no job left over

				double makespan = 0;
				for (int j = 0; j < CS.length; j += 3) {
					if (CS[j + 2] != -1) {
						makespan = Math.max(makespan,
								CS[j + 2] * T + sizes[CS[j]] * S);
					}
				}
				dictionary.put(CS, makespan);
				return makespan;

			} else { // not in dictionary and at least one job to set

				double min_F = Double.MAX_VALUE;

				for (int i = 0; i < CS.length; i += 3) {

					int[] CS_new;

					if (CS[i + 1] > 0) {
						CS_new = Arrays.copyOf(CS, CS.length);
						CS_new[i + 1] -= 1; // remove job by decreasing amount
					} else {
						continue;
					}

					int[] local_choices = Arrays.copyOfRange(choices, rec_level,
							choices.length); // save recorded choices

					double t = 0; // set job into schedule at earliest time

					for (int j = 0; j < CS.length; j += 3) {

						if (CS[j + 2] != -1) {
							t = Math.max(t, CS[j + 2] * T
									+ Math.min(sizes[CS[j]], sizes[CS[i]]) * S);
						}
					}
					CS_new[i + 2] = (int) Math.ceil(t / T); // save time slot

					double F_i = F(CS_new, rec_level + 1);

					if (min_F > F_i) {
						min_F = F_i;
						choices[rec_level] = CS[i]; // remember selection
					} else {
						// set back from saved if choice was not better
						for (int c = 0; c < local_choices.length; c++) {
							choices[rec_level + c] = local_choices[c];
						}
					}
				}
				dictionary.put(CS, min_F);
				return min_F;
			}
		}
	}
}
