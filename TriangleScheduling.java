import java.util.LinkedList;
import java.util.List;
import org.apache.commons.cli.*;

/* implemention of algorithms for the triangle scheduling problem */
class TriangleScheduling {

	/* use apache command line interpreter */
	private static CommandLine init_cli(String[] args) {

		CommandLineParser parser = new GnuParser();
		Options options = new Options();
		Option opt = new Option("b", "brute-force", false,
				"with brute-force algorithm");
		options.addOption(opt);

		opt = new Option("g", "greedy", false, "with greedy algorithm");
		options.addOption(opt);

		opt = new Option("d", "dp", true, "with dp algorithm");
		opt.setArgs(1);
		opt.setArgName("eps");
		options.addOption(opt);

		opt = new Option("j", "jobs", true,
				"comma seperated list of jobs as input\n"
						+ "only with brute-force: job as circle C_,"
						+ "as equilateral E_, as rectangular (_:_) ");
		opt.setArgs(1);
		opt.setArgName("p1,p2,...");
		options.addOption(opt);

		opt = new Option("t", "test", true,
				"m testcases with n uniformly distributed jobs");
		opt.setArgs(2);
		opt.setArgName("n, m");
		options.addOption(opt);

		CommandLine cmd = null;
		try {
			cmd = parser.parse(options, args);

		} catch (ParseException e) {
			System.out.println(e.getMessage());
			System.exit(1);
			return null;
		}

		if (!cmd.hasOption("test") && !cmd.hasOption("jobs")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("TriangleScheduling", "", options,
					"\nhttps://git.imp.fu-berlin.de/gross1989/TSImplementierung");
		}

		return cmd;
	}

	/*
	 * compute and visualize schedules for jobs from command line or generate
	 * test cases for the greedy algorithm
	 */
	public static void main(String[] args) {

		CommandLine cmd = init_cli(args);

		if (cmd.hasOption("test")) {

			String[] values = cmd.getOptionValues("test");
			int n = Integer.parseInt(values[0]);
			int m = Integer.parseInt(values[1]);
			generate_test_cases(n, m);

		} else if (cmd.hasOption("jobs")) {

			String[] input = cmd.getOptionValue("jobs").split(",");
			List<Job> jobs = new LinkedList<>();

			try {
				for (String inp : input) {

					if (inp.matches("\\(.+:.+\\)")) {
						String[] para = inp.substring(1, inp.length() - 1)
								.split(":");
						double w = Double.parseDouble(para[0]);
						double h = Double.parseDouble(para[1]);
						jobs.add(new JobRectangular(w, h));

					} else if (inp.matches("E.+")) {
						double s = Double.parseDouble(inp.substring(1));
						jobs.add(new JobEquilateral(s));

					} else if (inp.matches("C.+")) {
						double r = Double.parseDouble(inp.substring(1));
						jobs.add(new JobCircle(r));

					} else {
						double s = Double.parseDouble(inp);
						jobs.add(new JobIsosceles(s));
					}
				}

			} catch (NumberFormatException nfe) {
				System.out.println("input error: " + nfe.getMessage());
			}

			// System.out.println(Arrays.toString(jobs));
			ViewSVG svg = new ViewSVG();
			ViewSwing swing = new ViewSwing();

			for (Option o : cmd.getOptions()) {

				Schedule sched = null;

				if (o.getOpt().equals("b")) { // run brute-force algorithm

					long t_0 = System.nanoTime();
					sched = AlgoBF.run(jobs.toArray(new Job[0]));
					double cpu_time = (System.nanoTime() - t_0) * 1e-9;

					svg.add("Brute-Force", sched, cpu_time);
					swing.add("Brute-Force", sched, cpu_time);

				} else if (o.getOpt().equals("g")) { // run greedy algorithm

					List<JobIsosceles> isosceles_only = new LinkedList<>();
					for (Job j : jobs) {
						if (j instanceof JobIsosceles) {
							isosceles_only.add((JobIsosceles) j);
						}
					}
					long t_0 = System.nanoTime();
					sched = AlgoGreedy
							.run(isosceles_only.toArray(new JobIsosceles[0]));
					double cpu_time = (System.nanoTime() - t_0) * 1e-9;

					svg.add("Greedy", sched, cpu_time);
					swing.add("Greedy", sched, cpu_time);

				} else if (o.getOpt().equals("d")) { // run dp algorithm

					List<JobIsosceles> isosceles_only = new LinkedList<>();
					for (Job j : jobs) {
						if (j instanceof JobIsosceles) {
							isosceles_only.add((JobIsosceles) j);
						}
					}
					double eps = Double.parseDouble(o.getValue());
					long t_0 = System.nanoTime();
					sched = AlgoDP.run(
							isosceles_only.toArray(new JobIsosceles[0]), eps);
					double cpu_time = (System.nanoTime() - t_0) * 1e-9;

					svg.add("DP", sched, cpu_time, eps);
					swing.add("DP", sched, cpu_time, eps);
				}
			}

			swing.drawAll();
			svg.drawAll();
		}
	}

	/*
	 * compare brute-force and greedy schedueles for uniformly distributed job
	 * sizes for n jobs and with m iterations
	 */
	private static void generate_test_cases(int n, int m) {

		JobIsosceles[] jobs = new JobIsosceles[n];

		for (int i = 0; i < m; i++) {
			System.out.println("" + (i + 1) + " of " + m);

			for (int j = 0; j < n; j++) {
				jobs[j] = new JobIsosceles(Math.ceil(1 + Math.random() * 99));
			}

			if (Schedule.bt_ratio(jobs) <= 2) {
				System.out.println("skip");
				i--;
				continue;
			}

			Schedule bf = AlgoBF.run(jobs);
			Schedule greedy = AlgoGreedy.run(jobs);

			if (greedy.makespan() > bf.makespan()) {

				System.out.println("show");
				ViewSVG.file = "n_" + n + "_" + i + ".svg";
				ViewSVG svg = new ViewSVG();
				svg.add("BF", bf, -1);
				svg.add("Greedy", greedy, -1);
				svg.drawAll();
			}

			ViewCSV.add(bf, greedy, jobs);
		}
	}
}