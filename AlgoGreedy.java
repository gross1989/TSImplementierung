import java.util.Arrays;
import java.util.Collections;

/* greedy algorithm: place biggest job in biggest gap */
class AlgoGreedy {

	static Schedule run(JobIsosceles[] jobs) {

		Arrays.sort(jobs, Collections.reverseOrder()); // descending size
		Schedule sched = new Schedule(jobs);

		for (int i = 1; i < sched.n; i++) {

			int pos = i; // biggest gap either at the current end of schedule
			double gap = sched.makespan(i) - sched.start[i - 1];

			for (int j = 1; j < i; j++) { // or in between
				if (sched.start[j] - sched.start[j - 1] > gap) {
					gap = sched.start[j] - sched.start[j - 1];
					pos = j;
				}
			}

			Job tmp = sched.list[i];
			for (int k = i; k > pos; k--) {
				sched.list[k] = sched.list[k - 1]; // shift right following jobs
			}
			sched.list[pos] = tmp;

			sched.set(pos, i + 1); // update changed job positions
		}

		return sched;
	}
}