import java.awt.Polygon;

/* rectangular shaped job */
class JobRectangular extends Job {

	double w; // horizontal cathetus
	double h; // vertical cathetus

	JobRectangular(double width, double height) {
		w = width;
		h = height;
	}

	@Override
	/* minimal difference between this rectangular and another job b */
	public double diff(Job b) {

		if (b instanceof JobIsosceles) {
			return Job.diffSlopeVertical(w, h, ((JobIsosceles) b).s);

		} else if (b instanceof JobRectangular) {
			return Job.diffSlopeVertical(w, h, ((JobRectangular) b).h);

		} else if (b instanceof JobEquilateral) {

			double min = Job.diffSlopeVertical(w, h, ((JobEquilateral) b).h);
			double min2 = w - .5 * ((JobEquilateral) b).s
					+ Job.diffSlopeVertical(.5 * ((JobEquilateral) b).s,
							((JobEquilateral) b).h, h);

			return Math.min(min, min2);

		} else if (b instanceof JobCircle) {
			double b_r = ((JobCircle) b).r;
			return Job.diffSlopeCircle(w, h, b_r);
		}

		return 0;
	}

	@Override
	public String toString() {
		return String.format("(%f,%f)", h, w);
	}

	public Polygon visual(double scale) {

		int[] xs = {0, 0, (int) (w * scale)};
		int[] ys = {0, -(int) (h * scale), -(int) (h * scale)};

		return new Polygon(xs, ys, 3);
	}
}