import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

/* output (append) to file for comparing two schedules by makespan */
class ViewCSV {

	final static String file = "TSOutput.csv";

	public static void add(Schedule s1, Schedule s2, Job[] jobs) {

		try {
			PrintWriter output = new PrintWriter(new FileWriter(file, true));

			output.printf("%.5f\t%.5f\t", s1.makespan(), s2.makespan());
			output.printf("%.5f\t", s2.makespan() / s1.makespan());

			output.printf("%d\t%s%s", jobs.length, Arrays.toString(jobs),
					System.lineSeparator());

			output.close();

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}