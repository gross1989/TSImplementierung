/* data type representing a job with a certain size and a difference operation */
abstract class Job {

	/*
	 * minimal difference from this job to another either in the scheduling
	 * sense as start time difference or geometrically as difference between
	 * bottom-left corners of the figures
	 */
	abstract double diff(Job b);

	/* textual description */
	public abstract String toString();

	/* minimal difference from increasing line to a vertical with height */
	static double diffSlopeVertical(double x, double y, double h) {

		if (h >= y) {
			return x;
		} else {
			return x / y * h;
		}
	}

	/* minimal difference from circle (bottom-left) to a vertical with height */
	static double diffCircleVertical(double r, double h) {

		if (h >= r) {
			return 2 * r;

		} else { // below circle
			double d = r - h;
			return r + Math.sqrt(r * r - d * d);
		}
	}

	/* minimal difference from increasing line to a circle */
	static double diffSlopeCircle(double x, double y, double r) {

		double s = Math.sqrt(y * y + x * x);
		double h = x / s * r;

		if (y >= r + h) { // big enough to be tangential to circle
			double d = Math.sqrt(r * r - h * h);
			return x / y * (h + r) + d - r;

		} else {
			h = y - r;
			double d = Math.sqrt(r * r - h * h);
			return x + d - r;
		}
	}
}