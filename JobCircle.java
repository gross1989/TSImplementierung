/* circle shaped job */
class JobCircle extends Job {

	double r; // radius

	JobCircle(double d) {
		r = .5 * d;
	}

	@Override
	public String toString() {
		return String.format("(%f)", r);
	}

	@Override
	/* minimal difference between this circle and another job b */
	public double diff(Job b) {

		if (b instanceof JobIsosceles) {
			return Job.diffCircleVertical(r, ((JobIsosceles) b).s);

		} else if (b instanceof JobRectangular) {
			return Job.diffCircleVertical(r, ((JobRectangular) b).h);

		} else if (b instanceof JobEquilateral) {
			return 2 * r + b.diff(this) - ((JobEquilateral) b).s;

		} else if (b instanceof JobCircle) {
			double b_r = ((JobCircle) b).r;
			double x = r + b_r;
			double y = b_r - r;
			return r - b_r + Math.sqrt(x * x - y * y);
		}

		return 0;
	}
}