import java.awt.Polygon;
import java.util.LinkedList;
import java.util.List;

/* viewer for drawing several schedules with their jobs */
abstract class View {

	List<Schedule> list_schedule = new LinkedList<>();
	List<String> list_name = new LinkedList<>();
	List<Double> list_cpu_time = new LinkedList<>();
	List<Double> list_eps = new LinkedList<>();
	double org_width = 0; // original sizes
	double org_height = 0;
	double scale = 1;
	int width = 111; // window sizes
	int height = 111;

	abstract void drawString(String s, int x_pos, int y_pos);
	abstract void drawPolygon(Polygon p);
	abstract void drawCircle(int x_pos, int y_pos, int r);

	/* add a schedule to be drawn without epsilon value */
	void add(String name, Schedule plan, double cpu_time) {

		add(name, plan, cpu_time, 0);
	}

	void add(String name, Schedule sched, double cpu_time, double eps) {

		list_name.add(name);
		list_schedule.add(sched);
		list_cpu_time.add(cpu_time);
		list_eps.add(eps);

		org_width = Math.max(org_width,
				(int) Math.ceil(sched.makespan() * 1.0));
		org_height += height_schedule(sched);

		scale = 50.0 * sched.n / org_width;
		width = Math.max(480, (int) (org_width * scale));
		height = Math.max(100,
				(int) (org_height * scale + list_schedule.size() * 15));
		
	}

	/* draw each individual job */
	void drawJob(Job job, int x_pos, int y_pos) {

		if (job instanceof JobCircle) {
			int r = (int) (((JobCircle) job).r * scale);
			drawCircle(x_pos + r, y_pos - r, r);
			drawString(Double.toString(((JobCircle) job).r * 2), x_pos + r,
					y_pos - r + 13);

		} else {

			Polygon p = null;
			String s = "";
			int texty = 0; // position for job size info
			int textx = 0;

			if (job instanceof JobIsosceles) {
				p = ((JobIsosceles) job).visual(scale);
				texty = (int) (((JobIsosceles) job).s * scale);
				textx = 2;
				s = Double.toString(((JobIsosceles) job).s);

			} else if (job instanceof JobRectangular) {
				p = ((JobRectangular) job).visual(scale);
				texty = (int) (((JobRectangular) job).h * scale);
				textx = 2;
				s = Double.toString(((JobRectangular) job).h);

			} else if (job instanceof JobEquilateral) {
				p = ((JobEquilateral) job).visual(scale);
				texty = (int) (((JobEquilateral) job).h * scale);
				textx = (int) (((JobEquilateral) job).s / 2 * scale);
				s = Double.toString(((JobEquilateral) job).s);
			}

			p.translate(x_pos, y_pos);
			drawPolygon(p);
			drawString(s, x_pos + textx, y_pos - texty + 13);
		}
	}

	/* draw a schedule without epsilon value */
	int drawSchedule(String name, int y_pos, Schedule plan, double cpu_time) {
		return drawSchedule(name, y_pos, plan, cpu_time, 0);
	}

	int drawSchedule(String name, int y_pos, Schedule sched, double cpu_time,
			double eps) {

		y_pos += height_schedule(sched) * scale;

		for (int i = 0; i < sched.n; i++) {

			int x_pos = (int) (sched.start[i] * scale);
			Job job = sched.list[i];

			drawJob(job, x_pos, y_pos);
		}

		drawString(String.format("%s", name), 0, y_pos + 13);

		drawString(String.format("makespan = %.3f", sched.makespan()), 100,
				y_pos + 13);

		if (cpu_time > 0) {
			drawString(String.format("cpu = %.3f s", cpu_time), 250,
					y_pos + 13);
		}

		if (eps > 0) {
			drawString(String.format("eps = %.3f", eps), 400, y_pos + 13);
		}

		return y_pos + 15;
	}

	/* draw all added schedules */
	void drawAll() {
		int y_pos = 0;
		for (int i = 0; i < list_schedule.size(); i++) {
			y_pos = drawSchedule(list_name.get(i), y_pos, list_schedule.get(i),
					list_cpu_time.get(i), list_eps.get(i));
		}
	}

	/* vertical range of a schedule by finding biggest job */
	private int height_schedule(Schedule sched) {

		int max_h = 0;

		for (Job j : sched.list) {
			double size_vertical = 0;
			if (j instanceof JobIsosceles) {
				size_vertical = ((JobIsosceles) j).s;

			} else if (j instanceof JobRectangular) {
				size_vertical = ((JobRectangular) j).h;

			} else if (j instanceof JobEquilateral) {
				size_vertical = ((JobEquilateral) j).h;

			} else if (j instanceof JobCircle) {
				size_vertical = ((JobCircle) j).r * 2;
			}
			max_h = Math.max(max_h, (int) size_vertical);
		}
		return max_h;
	}
}