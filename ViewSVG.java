import java.awt.Polygon;
import java.io.IOException;
import java.io.PrintWriter;

/* output as SVG image */
class ViewSVG extends View {
	static String file = "TSOutput.svg";
	PrintWriter output;

	@Override
	void drawString(String s, int x_pos, int y_pos) {

		output.println(String.format("<text x=\"%d\" y=\"%d\">%s</text>", x_pos,
				y_pos, s));
	}

	@Override
	void drawPolygon(Polygon p) {

		output.print("<polygon points=\"");

		for (int i = 0; i < p.npoints; i++) {
			output.print(String.format(" %d,%d", p.xpoints[i], p.ypoints[i]));
		}

		output.println(
				"\" style=\"fill:white;stroke:black;stroke-width:1\" />");
	}

	@Override
	void drawCircle(int x_pos, int y_pos, int r) {

		output.print(String.format("<circle cx= \"%d\" cy=\"%d\" r=\"%d\"",
				x_pos, y_pos, r));
		output.println(" style=\"fill:white;stroke:black;stroke-width:1\" />");
	};

	@Override
	void drawAll() {

		try {
			output = new PrintWriter(file, "UTF-8");

			output.println("<?xml version=\"1.0\"?>");
			output.println("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" "
					+ "\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">");
			output.println(
					"<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" "
							+ "font-family=\"sans-serif\" font-size=\"10pt\" width=\""
							+ width + "\" height=\"" + height + "\" >");
			output.println(
					"<rect width=\"100%\" height=\"100%\" fill=\"#FFFFFF\"/>");

			super.drawAll();

			output.println("</svg>");

			output.close();

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}