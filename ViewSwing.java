import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JFrame;
import javax.swing.JPanel;

/* draw in window with Swing */
class ViewSwing extends View {

	private Graphics gra;

	@Override
	void drawString(String s, int x_pos, int y_pos) {
		gra.drawString(s, x_pos, y_pos);
	}

	@Override
	void drawPolygon(Polygon p) {
		gra.drawPolygon(p);
	};

	@Override
	void drawCircle(int x_pos, int y_pos, int r) {
		gra.drawOval(x_pos - r, y_pos - r, 2 * r, 2 * r);
	}

	@Override
	void drawAll() {

		javax.swing.SwingUtilities.invokeLater(new Runnable() { // own thread

			@Override
			public void run() {
				JFrame frame = new JFrame("Triangle Scheduling");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				JPanel panel = new JPanel() { // own panel

					private static final long serialVersionUID = 123456789L;

					public void paintComponent(Graphics g) {
						super.paintComponent(g);

						Dimension d = getSize();
						g.setColor(Color.WHITE);
						g.fillRect(0, 0, d.width, d.height);
						g.setColor(Color.BLACK);

						gra = g; // set Graphics attribute of ViewSwing object
						ViewSwing.super.drawAll(); // begin drawing
					}
				};

				panel.setPreferredSize(new Dimension(width, height));
				frame.getContentPane().add(panel);
				frame.pack();
				frame.setVisible(true);
			}
		});
	}
}
